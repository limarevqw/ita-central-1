<?php

namespace App\Controller;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/ping", name="app_index")
     */
    public function pingAction()
    {
        return new JsonResponse(['result' => 'pong']);
    }

    /**
     * @Route("/client/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportAndEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/get-one-data-client/{password}/{email}", name="app_get_one_data_client")
     * @Method("GET")
     * @param string $password
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function getOneDataClientAction(
        string $password,
        string $email,
        ClientRepository $clientRepository)
    {
        $result = $clientRepository->findOneByPasswordAndEmail($password, $email);

        $data = null;
        $status = false;

        if ($result) {
            $data = $result->toArray();
            $status = true;
        }
        return new JsonResponse([
            'data' => $data,
            'status' => $status
        ]);
    }

    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');

        if(empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: '.var_export($data,1)],406);
        }

        if ($clientRepository->findOneByPassportAndEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'],406);
        }

        $client = $clientHandler->createNewClient($data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/get-one-soc-data-client/{uid}/{email}", name="app_get_one_soc_data_client")
     * @Method("GET")
     * @param string $uid
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     * @internal param string $password
     */
    public function getOneSocDataClientAction(
        string $uid,
        string $email,
        ClientRepository $clientRepository)
    {
        $result = $clientRepository->findOneByUidAndEmail($uid, $email);

        $data = null;
        $status = false;

        if ($result) {
            $data = $result->toArray();
            $status = true;
        }
        return new JsonResponse([
            'data' => $data,
            'status' => $status
        ]);
    }

    /**
     * @Route("/create-client-soc", name="app_create_soc_client")
     * @Method("POST")
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createSocClientAction(
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['original_city'] = $request->get('original_city');
        $data['last_name'] = $request->get('last_name');
        $data['email'] = $request->get('email');
        $data['first_name'] = $request->get('first_name');
        $data['identity'] = $request->get('identity');
        $data['verified_email'] = $request->get('verified_email');
        $data['profile'] = $request->get('profile');
        $data['uid'] = $request->get('uid');
        $data['network'] = $request->get('network');

        $client = $clientHandler->createNewSocUser($data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }
}
