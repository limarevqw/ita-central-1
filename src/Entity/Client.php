<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Clent\ClentInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=254, unique=true, nullable=true)
     */
    private $passport;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $originalCity;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $identity;

    /**
     * @ORM\Column(type="smallint", length=2, nullable=true)
     */
    private $verifiedEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $profileUrl;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $uid;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $network;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    public function __construct()
    {
        $this->registeredAt = new \DateTime("now");
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt(): ?\DateTime
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     */
    public function setRegisteredAt(\DateTime $registeredAt): void
    {
        $this->registeredAt = $registeredAt;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param mixed $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $passport
     * @return Client
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param mixed $originalCity
     * @return Client
     */
    public function setOriginalCity($originalCity)
    {
        $this->originalCity = $originalCity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginalCity()
    {
        return $this->originalCity;
    }

    /**
     * @param mixed $lastName
     * @return Client
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $firstName
     * @return Client
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $identity
     * @return Client
     */
    public function setIdentity($identity)
    {
        $this->identity = $identity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * @param mixed $verifiedEmail
     * @return Client
     */
    public function setVerifiedEmail($verifiedEmail)
    {
        $this->verifiedEmail = $verifiedEmail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerifiedEmail()
    {
        return $this->verifiedEmail;
    }

    /**
     * @param mixed $profileUrl
     * @return Client
     */
    public function setProfileUrl($profileUrl)
    {
        $this->profileUrl = $profileUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileUrl()
    {
        return $this->profileUrl;
    }

    /**
     * @param mixed $uid
     * @return Client
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $network
     * @return Client
     */
    public function setNetwork($network)
    {
        $this->network = $network;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNetwork()
    {
        return $this->network;
    }

    public function toArray()
    {
        return [
            'password' => $this->getPassword(),
            'email' => $this->getEmail(),
            'passport' => $this->getPassport(),
            'original_city' => $this->getOriginalCity(),
            'last_name' => $this->getLastName(),
            'first_name' => $this->getFirstName(),
            'identity' => $this->getIdentity(),
            'verified_email' => $this->getVerifiedEmail(),
            'profile' => $this->getProfileUrl(),
            'uid' => $this->getUid(),
            'network' => $this->getNetwork()
        ];
    }


}
