<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\Client;

use App\Entity\Client;

class ClientHandler
{
    /**
     * @param array $data
     * @return Client
     */
    public function createNewClient(array $data) {
        $client = new Client();
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $password = $this->encodePassword($data['password']);
        $client->setPassword($password);

        return $client;
    }

    /**
     * @param array $data
     * @return Client
     */
    public function createNewSocUser(array $data)
    {
        $client = new Client();
        $client->setOriginalCity($data['original_city']);
        $client->setLastName($data['last_name']);
        $client->setEmail($data['email']);
        $client->setFirstName($data['first_name']);
        $client->setIdentity($data['identity']);
        $client->setVerifiedEmail($data['verified_email']);
        $client->setProfileUrl($data['profile']);
        $client->setUid($data['uid']);
        $client->setNetwork($data['network']);
        $password = $this->encodePassword(rand(1,1000));
        $client->setPassword($password);

        return $client;
    }

    public function encodePassword($password)
    {
        return md5($password).md5($password.'2');
    }
}
